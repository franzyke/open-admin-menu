<?php

use Illuminate\Support\Facades\Route;
use Lailen\Menus\Http\Controllers\MenusController;

Route::get('menus/{menu}/items', [MenusController::class, 'editItems']);
Route::get('menus/{menu}/items/{menuItem}/edit', [MenusController::class, 'editItem']);
Route::post('menus/{menu}/items', [MenusController::class, 'editItemsOrder']);
Route::post('menus/{menu}/store-item', [MenusController::class, 'storeItem']);
Route::put('menus/{menu}/items/{menuItem}/update-item', [MenusController::class, 'updateItem']);
Route::resource('menus', MenusController::class);
