<?php

namespace Lailen\Menus;

use OpenAdmin\Admin\Extension;

class Menus extends Extension
{
    public $name = 'menus';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';

    public $menu = [
        'title' => 'Menus',
        'path'  => 'menus',
        'icon'  => 'icon-cogs',
    ];
}
