<?php

namespace Lailen\Menus\Actions;

use OpenAdmin\Admin\Actions\RowAction;

class EditMenuItems extends RowAction
{
    public $name = 'edit-menu-items';

    public $icon = 'icon-edit';

    public function href()
    {
        return '/admin/menus/' . $this->getKey() . '/items';
    }
}
