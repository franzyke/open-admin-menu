<?php

namespace Lailen\Menus;

use Illuminate\Support\ServiceProvider;

class MenusServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(Menus $extension)
    {
        if (! Menus::boot()) {
            return ;
        }

        if ($views = $extension->views()) {
            $this->loadViewsFrom($views, 'menus');
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/lailen/menus')],
                'menus'
            );
        }

        if ($this->app->runningInConsole()) {
            $this->publishes(
                [__DIR__.'/../database/migrations' => database_path('migrations')],
                'menus'
            );
        }

        $this->app->booted(function () {
            Menus::routes(__DIR__.'/../routes/web.php');
        });
    }
}
